#include <TM1637.h>
#include <Servo.h>

#define PHOTO_RESISTOR_PIN A0
#define PHOTO_RESISTOR_THRESHOLD 200
#define SERVO_PIN 9

#define DISPLAY1_CLK_PIN 2//pins definitions for TM1637 and can be changed to other ports
#define DISPLAY1_DIO_PIN 3
TM1637 tm1637(DISPLAY1_CLK_PIN,DISPLAY1_DIO_PIN);

boolean timerGestart = false;
double startMoment;
double eindMoment;

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int servoPos = 0; 

void setup() {
  Serial.begin(9600);
  pinMode(LASER_PIN, OUTPUT);
  pinMode(PHOTO_RESISTOR_PIN, INPUT);
  pinMode(13, OUTPUT);

  tm1637.init();
  tm1637.set(BRIGHT_TYPICAL);//BRIGHT_TYPICAL = 2,BRIGHT_DARKEST = 0,BRIGHTEST = 7;

  myservo.attach(SERVO_PIN);
  servoDown();
  telAf(3);
  startRace();
}

void startRace(){
  servoUp();
  startTimer();
}

void loop() {
  if(isLijnOnderbroken()){
    Serial.println("Lijn onderbroken");
    int verstrekenSecondenEnTienden = bepaalVerstrekenSecondenEnTienden();
    stopTimer();
    for(int i=0; i<3; i++){
      tm1637.displayNum(verstrekenSecondenEnTienden);
      delay(500);
      tm1637.clearDisplay();
      delay(500);
    }
    tm1637.displayNum(verstrekenSecondenEnTienden);
    servoDown();
    delay(1000);
    myservo.detach();
    while(true){
      
    }
  } else if(timerGestart){
    int verstrekenSecondenEnTienden = bepaalVerstrekenSecondenEnTienden();
    Serial.println(verstrekenSecondenEnTienden);
    tm1637.displayNum(verstrekenSecondenEnTienden);
    delay(100);
//  } else if(isGestart()){
//    startTimer();
  }

  
}
boolean isGestart() {
  return true;
}


boolean isLijnOnderbroken() {
  int state = analogRead(PHOTO_RESISTOR_PIN);
//  Serial.print("State: ");
//  Serial.println(state);
  return state < PHOTO_RESISTOR_THRESHOLD;
//  return millis() > 3000;
}

void startTimer(){
  timerGestart = true;
  startMoment = millis();
}

void stopTimer(){
  timerGestart = false;
  eindMoment = millis();
}

int bepaalVerstrekenSecondenEnTienden(){
  unsigned long verstrekenMillis = millis() - startMoment;
  return verstrekenMillis / 100;
}

void servoUp(){
  myservo.write(70);
}

void servoDown(){
  myservo.write(157);
}

void telAf(int seconden){
  for(int i=seconden; i>0; i--){
    tm1637.displayNum(i*1111);
    delay(1000);
  }
}
