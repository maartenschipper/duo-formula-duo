# DUO Formula DUO

**Pins**

| Device | Arduino | Kleur |
| ------ | ------ | ------ |
| Servo (Start) - Signal | 9 | Geel |
| Klok - CLK | 2 | Wit |
| Klok - DIO | 3 | Geel |
| PhotoResistor (Finish) | A0 | |
